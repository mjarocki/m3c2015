"""Python module to compute
sqrt function with Newton's method"""

def sqrt2(a,debug=False):
    """function to compute sqrt(a) with
    Newton's method"""

    #check input
    assert type(a) is int or type(a) is float, "error, a must be a number"
    assert a>=0, "error, a must be non-negative"


    x0 = 1.0 #initial guess for sqrt(a)

    imax = 10000 #maximum number of iterations
    tol = 1.0e-12 #convergence criteria
    
    #loop implementing Newton's method
    for i in range(imax):
        x1 = a/(2*x0) + x0/2
        deltax = abs(x0-x1)
        if debug:
            print "after iteration %d, delta x= %16.12f" %(i+1,deltax)
        if deltax<tol:
            if debug:
                print "converged"
            break
        x0 = x1
    
    return x1    

"""To run this function from the ipython terminal: 
import mysqrt1
mysqrt1.sqrt2(a)

"""
